$(document).ready(function () {
  // Show pop-up-menu for LogIn and registrtation
  var wrapPopUp = $('#wrap-pop-up'),
    popUpMenu = $('#pop-up-menu');
  $('.login').click(function (event) {
    event.preventDefault();
    $(wrapPopUp).show();
    $(popUpMenu).show();
  });
  // Hide pop-up-menu for LogIn and registrtation
  $('#close-pop-up-menu, #wrap-pop-up').click(function (event) {
    event.preventDefault();
    wrapPopUp.hide();
    popUpMenu.hide();
  });
  $(document).keyup(function (e) {
    if (e.keyCode === 27) {
      $(popUpMenu).hide();
      $(wrapPopUp).hide();
    }
  });
  // Flikr widget
  $('.flickr-widget').find('img').click(function () {
    var popUpImage = this;

    $(wrapPopUp).show();
    $(popUpImage).addClass('pop-up-image');
    $(wrapPopUp).click(function () {
      $(popUpImage).removeClass('pop-up-image');
      $(wrapPopUp).hide();
    });
    $(document).keyup(function (e) {
      if (e.keyCode === 27) {
        $(popUpImage).removeClass('pop-up-image');
        $(wrapPopUp).hide();
      }
    });
  });
  // Close any pop p by pressing ESC

  // News blog swap

  // Slider switch buttons
  $('.banner-button').eq(0).click(function () {
    $('.banner-img').removeClass('active');
    $('.active-slider').removeClass('active-button');
    $('.banner-img').eq(0).addClass('active');
    $('.active-slider').eq(0).addClass('active-button');
  });
  $('.banner-button').eq(1).click(function () {
    $('.banner-img').removeClass('active');
    $('.active-slider').removeClass('active-button');
    $('.banner-img').eq(1).addClass('active');
    $('.active-slider').eq(1).addClass('active-button');
  });
  $('.banner-button').eq(2).click(function () {
    $('.banner-img').removeClass('active');
    $('.active-slider').removeClass('active-button');
    $('.banner-img').eq(2).addClass('active');
    $('.active-slider').eq(2).addClass('active-button');
  });
  // Auto-Slider
  var sliderDelay = 3000,
    numberOfSlides = 3,
    currentSlide = 0;

  var timer = setTimeout(function () {
    autoSlider()
  }, sliderDelay);

  function autoSlider() {
    $('.banner-img').removeClass('active');
    $('.active-slider').removeClass('active-button');
    $('.banner-img').eq(currentSlide).addClass('active');
    $('.active-slider').eq(currentSlide).addClass('active-button');
    currentSlide++;
    if (currentSlide === numberOfSlides) {
      currentSlide = 0;
    }
    setTimeout(function () {
      autoSlider()
    }, sliderDelay);
  }
  // Save adress to Local Cookie
  $('#email-button').click(function (event) {
    event.preventDefault();
    var emailAddress = $('#email-address').val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(emailAddress)) alert('Incorrect email');
    else {
      if (emailAddress == '') alert('Enter your email');
      localStorage.setItem('email', JSON.stringify(emailAddress));
    }
  });

  // Twitter Widget ready cheker
  ! function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
      p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
      js = d.createElement(s);
      js.id = id;
      js.src = p + "://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);
    }
  }(document, "script", "twitter-wjs");
  // Blog news
  var countNews = $('.wrap-news').eq();
  var divs = countNews.prevObject.length;
  var currentHideNews = 0;
  var currentShowNews = 2;
  
  setTimeout(function () { autoNews() }, 1000);
  
  function autoNews () {
      $('.wrap-news').eq(currentHideNews).hide();
      $('.wrap-news').eq(currentShowNews).show();
      currentShowNews++;
      currentHideNews++;
      if(currentHideNews == 3) {
        currentHideNews = 0;
      }
      if(currentShowNews == 3) {
        currentShowNews = 0;
      }
      
     setTimeout(function () { autoNews() }, 1000);
  }
  
  //    $(function () {      
  //        var $cont = $(".twitter-container"),
  //        prd = setInterval(function () {
  //            if ($cont.find("> iframe").contents().find(".twitter-timeline").length > 0) {
  //                var $body = $cont.find("> iframe").contents().find("body");
  //                clearInterval(prd)
  //                $body.attr("id", "twitterStyled")
  //                .append($("#twitterStyle"));
  //            }
  //        }, 100);
  //    });
  //  
});